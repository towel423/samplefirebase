<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Contract\Database;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class HomeController extends Controller
{
    public function __construct(Database $database)
    {
        $this->database = $database;
        $this->tablename = 'contacts';
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function users()
    {
        $contacts = $this->database->getReference($this->tablename)->getValue();
        $myCollectionObj = collect($contacts);
        $contacts = $this->paginate($myCollectionObj);

        $paginator = $contacts->links()->elements;

        return view('userdetails', compact('contacts','paginator'));
    }

    public function addusers()
    {
        return view('addusers');
    }

    public function postaddusers(Request $request)
    {
        $ref_tablename = 'contacts';
        $postData = $request->all();
        $postRef = $this->database->getReference($this->tablename)->push($postData);

        return redirect()->route('listusers');
    }

    public function editusers($id)
    {
        $key = $id;
        $contacts = $this->database->getReference($this->tablename)->getChild($key)->getValue();

        if($contacts)
        {
            return view('editusers', compact('contacts','key'));
        }
        else
        {
            return redirect('/users');
        }  
    }

    public function posteditusers(Request $request,$id)
    {
        $key = $id;
        $updates = $request->all();
        $this->database->getReference($this->tablename.'/'.$key)->update($updates);
        return redirect('/users');
    }

    public function postdeleteusers($id)
    {
        $key = $id;
        $this->database->getReference($this->tablename.'/'.$key)->remove();
        return redirect('/users');
    }
    
}
