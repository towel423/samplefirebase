<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Laravel CRUD Operation Using Google Firebase</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container" style="margin-top: 50px;">
        <h4 class="text-center">Laravel CRUD Operation Using Google Firebase</h4><br>
        <h5># Edit User</h5>
        <div class="card card-default">
            <div class="card-body">
                <form id="addUser" class="form-inline" method="POST" action="{{url('post-edit-users/'.$key)}}">
                    @csrf
                    @method('PUT')
                    <div class="form-group mb-2">
                        <label for="name" class="sr-only">Name</label>
                        <input id="name" type="text" class="form-control" name="name" placeholder="Name"
                               required autofocus value="{{$contacts['name']}}">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="email" class="sr-only">Email</label>
                        <input id="email" type="text" class="form-control" name="email" placeholder="Email"
                               required autofocus value="{{$contacts['email']}}">
                    </div>
                    <button id="submitUser" type="submit" class="btn btn-primary mb-2">Save</button>
                </form>
            </div>
        </div>
        <br>
    </div>

    <!-- Update Model -->

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</body>
</html>