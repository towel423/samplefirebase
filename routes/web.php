<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('users', [App\Http\Controllers\HomeController::class, 'users'])->name('listusers');
Route::get('add-users', [App\Http\Controllers\HomeController::class, 'addusers'])->name('addusers');
Route::post('post-add-users', [App\Http\Controllers\HomeController::class, 'postaddusers'])->name('postaddusers');
Route::get('edit-users/{id}', [App\Http\Controllers\HomeController::class, 'editusers'])->name('editusers');
Route::put('post-edit-users/{id}', [App\Http\Controllers\HomeController::class, 'posteditusers'])->name('posteditusers');
Route::delete('post-delete-users/{id}', [App\Http\Controllers\HomeController::class, 'postdeleteusers'])->name('postdeleteusers');
